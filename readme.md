# Staging recreation

When re-creating the whole infrastrcucture, two things must be done first:
 - DB backup (snapshot)
 - File backup (rsync from EFS)

When destroying infrastructure with `terraform destroy` there may be issues with deleting the security groups. This is because terraform does not detect certain resource dependencies. For this infrastructure those dependencies are on the EFS and DB security groups. To be able to delete them, we must go into the resource in AWS and disasociate the sec group from the resource.

There may be a problem with deleting the db subnet group, this is because it tried to delete it when a database was not fully deleted itself. Running the `terraform destroy` command again should work fine.

There is an issue with restoring database from `.sql` file, there is a workaround:

When running `terraform apply` comment the database read-replicas and create the main database. The restore through MySQL Workbench will not work, we will have to restore from the snapshot to a database with a different name, for example `blackupstart-db-new`. When the snapshot is restored rename the database created by terraform to something like `blackupstart-db-old` and remove the `new` tag from the snapshot restored database. Then run terraform to apply all changes to the new database, then uncomment the read-replicas and run `terraform apply`.

Once everything is destroyed, run `terraform apply`.

Run `rsync` command to restore site files.

`rsync -av -e "ssh -i <path/to/key>" --exclude=".git/*" --rsync-path="sudo rsync" <path/to/code> <instance_user>@<instance_dns>:<dest/path/for/code>`

After restoring all the files, to be able to access the site through the dns name, we have to modify the Route53 record to point to the new Cloudfront Distribution.

# Putting a Wordpress site on AWS

## Infrastructure

Certain elements of the infrastructure might change according to necessity; this is not the only design for WP. The current architecture was designed with scalability and availability in mind. It consists of:

- Cloudfront distribution
- Application Load Balancer
- AutoScaling Group
- Elastic File System
- RDS and Read-Replicas

### Improvement opportunities

- Create one EFS mount point per subnet and make the autoscaling group instance detect subnets and connect to the corresponding mount point
- ~~Make everything dynamic through variables~~
- ~~Make read-replicas depend on variables~~
- Include Route53 config
- Include Cloudfront policies when Terraform implements it
- Create metric dashboard for monitoring
- RDS read replicas are not evenly distributed in AZs
- Create AMI to eliminate need for `user_data.sh` in template
- Implement memcache on site (https://aws.amazon.com/elasticache/memcached/wordpress-with-memcached/)
- Ability to import sql dump to database (Identify why it can't be done, https://aws.amazon.com/premiumsupport/knowledge-center/definer-error-mysqldump/, https://aws.amazon.com/premiumsupport/knowledge-center/error-1227-mysqldump/) 
- Protect load balancer (https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/restrict-access-to-load-balancer.html)
- Refactor, refactor, refactor...

## Database

The migration of the database is pretty straight-forward. Connect to the database using a MySQL client. For this case we used MySQL Workbench.

### Changing domain

If the URL of the site is going to change or there is going to be a temporary URL, it must be changed in the database. To change all occurrences of the old URL into the new URL, you can use the python script in this repo `wordpress_change_site_url.py`.

## Code

Before restoring the site's code to the EFS there are some changes that must be made.

There are a couple of potential problems that come with the infrastructure on which we are putting the site.

- Too many redirects
- Mixed content error (http-https)

The problem on too many redirects is caused by the lack of an SSL certificate on the site and it being transferred through HTTPS. If an SSL certificate is not possible, It is solved by going into the `wp-content/canonical.php` file and commenting out this section of the code:

    $redirect_url = apply_filters( 'redirect_canonical', $redirect_url, $requested_url );

	// Yes, again -- in case the filter aborted the request.
	if ( ! $redirect_url || strip_fragment_from_url( $redirect_url ) === strip_fragment_from_url( $requested_url ) ) {
	 	return;
	 }

	 if ( $do_redirect ) {
	 	// Protect against chained redirects.
	 	if ( ! redirect_canonical( $redirect_url, false ) ) {
	 		wp_redirect( $redirect_url, 301 );
	 		exit;
	 	} else {
	 		// Debug.
	 		// die("1: $redirect_url<br />2: " . redirect_canonical( $redirect_url, false ) );
	 		return;
	 	}
	 } else {
	 	return $redirect_url;
	 }

There is one other solution to this, but it does not always work. It is by adding this line to the wp-config.php file:

`if ($_SERVER[‘HTTP_X_FORWARDED_PROTO’] == ‘https’) $_SERVER[‘HTTPS’]=’on’;`
 
The other problem that might arise is that the CSS for the page is not loading due to a “mixed content” error. This is caused by the mix of HTTP and HTTPS on the infrastructure. It’s solved by adding the following line in the wp-config.php file:

`$_SERVER[‘HTTPS’]=’on’;`
 
The first code upload to the EFS must be done with all the site’s code. This is easily done with rsync through any of the instances connected to the EFS.

`rsync -av -e "ssh -i <path/to/key>" --exclude=".git/*" --rsync-path="sudo rsync" <path/to/code> <instance_user>@<instance_dns>:<dest/path/for/code>`

After this, the site files will be on the EFS and the site should work.
