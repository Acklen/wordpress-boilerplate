import sys
import mysql.connector

def main(args):
    try:
        connection = mysql.connector.connect(host=args[0],
                                database=args[1],
                                user=args[2],
                                password=args[3])

        cursor = connection.cursor(prepared=True)

        wp_upload_update = """UPDATE `wp_options` SET `option_value` = 'wp-content/uploads' WHERE (`option_id` = '50');"""
        wp_options_update = """UPDATE wp_options SET option_value = replace(option_value, %s, %s) WHERE option_name = 'home' OR option_name = 'siteurl';"""
        wp_posts_update = """UPDATE wp_posts SET guid = replace(guid, %s, %s);"""
        wp_posts_update_2 = """UPDATE wp_posts SET post_content = replace(post_content, %s, %s);"""
        wp_postsmeta_update = """UPDATE wp_postmeta SET meta_value = replace(meta_value, %s, %s);"""

        wp_url_tuple= (args[4], args[5])

        cursor.execute(wp_upload_update)
        cursor.execute(wp_options_update, wp_url_tuple)
        cursor.execute(wp_posts_update, wp_url_tuple)
        cursor.execute(wp_posts_update_2, wp_url_tuple)
        cursor.execute(wp_postsmeta_update, wp_url_tuple)
        
        connection.commit()
        print("DB update successful")

    except mysql.connector.Error as error:
        print("parameterized query failed {}".format(error))

if __name__ == "__main__":
    if len(sys.argv) != 7:
        print("Usage: python3 wordpress_change_site_url.py <dbhost> <dbname> <dbuser> <dbpass> <oldurl> <newurl>")
        sys.exit()
    main(sys.argv[1:])