#!/bin/bash

# updating native packages
yum update -y

# installing and activating apache
# yum install httpd -y
# systemctl start httpd
# systemctl enable httpd.service #starts on boot

# installing php
# amazon-linux-extras enable php7.3
# yum clean metadata
# yum install php-cli php-pdo php-fpm php-json php-mysqlnd -y

# install docker and docker-compose
# sudo yum update -y
# sudo amazon-linux-extras install docker
# sudo yum install docker
# sudo service docker start
# sudo systemctl enable docker.service
# sudo systemctl enable containerd.service
# sudo usermod -a -G docker ec2-user
# sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-`uname -s`-`uname -m` | sudo tee /usr/local/bin/docker-compose > /dev/null
# sudo chmod +x /usr/local/bin/docker-compose
# ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

# mounding EFS in /var/www/html
# mkdir /var/www/
# mkdir /var/www/html/
# mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${efs_mount_dns}:/ /var/www/html

# create docker-compose file and run container
# cd /var/www/
cat > docker-compose.yml << EOF
version: '3.3'

services:
  wordpress:
    restart: always
    image: wordpress:5.5.3
    logging:
      driver: awslogs
      options:
        awslogs-group: "docker-logs"
    volumes:
      - ./html:/var/www/html/
      - /home/:/home/
    ports:
      - "80:80"
    environment:
      WORDPRESS_DB_HOST: myblackreceipt-db.cjgpyjkt86jn.us-east-1.rds.amazonaws.com:3306
      WORDPRESS_DB_USER: myblackreceipt
      WORDPRESS_DB_PASSWORD: receiptblackmy
      WORDPRESS_DB_NAME: i7102863_wp2
EOF

# cat > /home/memcache_script.sh << EOF
# #!/bin/bash
# # cd /home
# # curl -s https://s3.amazonaws.com/elasticache-downloads/ClusterClient/PHP-7.0/latest-64bit > latest-64bit
# # tar -zxvf latest-64bit
# # mv artifact/amazon-elasticache-cluster-client.so /usr/local/lib/php/extensions/
# # echo "extension=amazon-elasticache-cluster-client.so" >> /usr/local/etc/php.ini-production
# # echo "extension=amazon-elasticache-cluster-client.so" >> /usr/local/etc/php.ini-development
# # rm -rf latest-64bit artifact
# apt-get update
# apt-get install -y libz-dev libmemcached-dev
# pecl install memcached
# docker-php-ext-enable memcached
# EOF

cat > /home/apache_script.sh << EOF1
#!/bin/bash
cat > /etc/apache2/mods-available/mpm_worker.conf << EOF
<IfModule mpm_worker_module>
ServerLimit 250
StartServers 10
MinSpareThreads 75
MaxSpareThreads 250
ThreadLimit 64
ThreadsPerChild 32
MaxRequestWorkers 8000
MaxConnectionsPerChild 10000
</IfModule>
EOF
EOF1

# chmod +x /home/memcache_script.sh
chmod +x /home/apache_script.sh

mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${efs_mount_dns}:/ /var/www/html
cd /var/www
docker-compose down
docker-compose up -d

# docker exec www_wordpress_1 /home/memcache_script.sh
docker exec www_wordpress_1 /home/apache_script.sh

# installing wordpress 5.5.3
# curl https://wordpress.org/wordpress-5.5.3.tar.gz -o wordpress.tar.gz
# tar --strip-components=1 -zxvf wordpress.tar.gz --directory /var/www/html/
# sudo chown -R apache:apache /var/www/html
# rm wordpress.tar.gz -f

# pull staging code
# cd /var/www/
# curl download_url --output staging.zip
# sudo unzip staging.zip -d html/
# sudo chown -R apache:apache html/
# sudo find html/ -type d -exec chmod 755 {} \;
# sudo find html/ -type f -exec chmod 644 {} \;
#sudo sed 's/\sDEFINER=`[^`]*`@`[^`]*`//g' -i mysql.sql
#mysql -u wordpress -p db_url < mysql.sql

#$_SERVER['HTTPS']='on';

# rsync -av -e "ssh -i ./blackupstart-key.pem" --exclude="node_modules/*" --rsync-path="sudo rsync" ../blackupstartwp/ ec2-user@ec2-3-139-240-204.us-east-2.compute.amazonaws.com:/var/www/html/

# removing apache homepage
# mv /etc/httpd/conf.d/welcome.conf /etc/httpd/conf.d/welcome.conf.bak
# systemctl restart httpd