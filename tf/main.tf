# terraform {
#   backend "s3" {
#     region = "us-east-1"
#   }
# }

provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

module "vpc" {
  source     = "./modules/vpc"
  project    = var.project
  aws_region = var.aws_region
}

module "rds_instance" {
  source      = "./modules/rds"
  vpc_id      = module.vpc.vpc_id
  db_ingress  = var.db_ingress
  vpc_cidr    = module.vpc.vpc_cidr
  subnets     = [module.vpc.public_subnet1_id, module.vpc.public_subnet2_id, module.vpc.public_subnet3_id]
  environment = var.environment
  project     = var.project
  db_user     = var.db_user
  db_pass     = var.db_pass
  db_name     = var.db_name
  read_replica_amount = var.read_replica_amount
  db_instance_type = var.db_instance_type
  monitoring_role_arn = var.monitoring_role_arn
}

module "load_balancer" {
  source      = "./modules/loadbalancer"
  # instance_id = module.ec2_instance.instance_id
  vpc_id      = module.vpc.vpc_id
  subnets     = [module.vpc.public_subnet1_id, module.vpc.public_subnet2_id, module.vpc.public_subnet3_id]
  environment = var.environment
  project     = var.project
}

module "ec2_instance" {
  source      = "./modules/ec2"
  subnet      = module.vpc.public_subnet1_id
  vpc_id      = module.vpc.vpc_id
  vpc_cidr    = module.vpc.vpc_cidr
  ec2_ingress = var.ec2_ingress
  lb_sg       = module.load_balancer.lb_id
  environment = var.environment
  project     = var.project
  lb_tg_arn   = module.load_balancer.target_group_arn
  region      = var.aws_region
  subnet_ids  = [module.vpc.public_subnet1_id, module.vpc.public_subnet2_id, module.vpc.public_subnet3_id]
  autoscaling_max_size = var.autoscaling_max_size
  autoscaling_min_size = var.autoscaling_min_size
  autoscaling_desired_capacity = var.autoscaling_desired_capacity
  instance_type = var.instance_type
  ami_id = var.ami_id
  key_name = var.key_name
  load_balancer_arn = module.load_balancer.load_balancer_arn
}

module "cloudfront_distro" {
  source      = "./modules/cloudfront"
  dns_name    = module.load_balancer.lb_dns
  environment = var.environment
  project     = var.project
  cert_arn    = var.cert_arn
  certificate_domain = var.certificate_domain
  cloudfront_aliases = var.cloudfront_aliases
}