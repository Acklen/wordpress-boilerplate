resource "aws_security_group" "db_sg" {
  name        = "${var.project}-DB-SG-${var.environment}"
  description = "Allow RDS to be accessed"
  vpc_id      = var.vpc_id

  ingress {
    description = "From IPs"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = var.db_ingress
  }

  ingress {
    description = "MYSQL from VPC"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project}-RDS-SG-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_db_parameter_group" "bupstart_db_pg" {
  name_prefix = "db-paramgroup-bupstart"
  family      = "mysql5.7"

  parameter {
    name  = "max_allowed_packet"
    value = "1073741824"
  }

  parameter {
    name = "log_bin_trust_function_creators"
    value = "1"
  }

  tags = {
    Name        = "${var.project}-DB-SubnetGroup-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_db_subnet_group" "bupstart_db_group" {
  name_prefix = "db-subnetgroup-bupstart"
  subnet_ids  = var.subnets

  tags = {
    Name        = "${var.project}-DB-SubnetGroup-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_db_instance" "wp_db" {
  identifier             = "${var.project}-db"
  skip_final_snapshot    = true
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = var.db_instance_type
  name                   = var.db_name
  username               = var.db_user
  password               = var.db_pass
  parameter_group_name   = aws_db_parameter_group.bupstart_db_pg.id
  vpc_security_group_ids = [aws_security_group.db_sg.id]
  publicly_accessible    = true
  db_subnet_group_name   = aws_db_subnet_group.bupstart_db_group.id
  multi_az               = true
  enabled_cloudwatch_logs_exports = [ "audit", "error", "general", "slowquery" ]
  apply_immediately = true
  monitoring_interval = 10
  monitoring_role_arn = var.monitoring_role_arn
  backup_retention_period = 3

  tags = {
    Name        = "${var.project}-DB-RDS-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_db_instance" "wp_db_replica" {
  count = var.read_replica_amount
  identifier             = "${var.project}-db-replica-${count.index}"
  skip_final_snapshot    = true
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = var.db_instance_type
  name                   = var.db_name
  username               = var.db_user
  password               = var.db_pass
  parameter_group_name   = aws_db_parameter_group.bupstart_db_pg.id
  vpc_security_group_ids = [aws_security_group.db_sg.id]
  publicly_accessible    = true
  # db_subnet_group_name   = aws_db_subnet_group.bupstart_db_group.id
  multi_az               = true
  enabled_cloudwatch_logs_exports = [ "audit", "error", "general", "slowquery" ]
  apply_immediately = true
  monitoring_interval = 10
  monitoring_role_arn = var.monitoring_role_arn
  replicate_source_db = "${var.project}-db"

  tags = {
    Name        = "${var.project}-DB-RDS-REPLICA-${count.index}-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}