variable "vpc_id" {
  description = "VPC ID for RDS"
}

variable "db_ingress" {
  description = "CIDR blocks for ingress to DB"
}

variable "subnets" {
  description = "Subnets for subnet group"
}

variable "vpc_cidr" {}
variable "environment" {}
variable "project" {}
variable "db_user" {}
variable "db_pass" {}
variable "db_name" {}
variable "read_replica_amount" {}
variable "db_instance_type" {}
variable "monitoring_role_arn" {}