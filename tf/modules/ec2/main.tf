resource "aws_security_group" "instance_sg" {
  name        = "${var.project}-Instance-SG-${var.environment}"
  description = "Allow HTTP in instance to be accessed"
  vpc_id      = var.vpc_id

  ingress {
    description     = "Allow HTTP for LoadBalancer"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [var.lb_sg]
  }

  ingress {
    description     = "Allow HTTPS for LoadBalancer"
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    security_groups = [var.lb_sg]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.ec2_ingress
  }

  ingress {
    description = "All in VPC"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project}-Instance-SG-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_security_group" "efs_sg" {
  name        = "${var.project}-EFS-SG-${var.environment}"
  description = "Allow connections inside VPC"
  vpc_id      = var.vpc_id

  ingress {
    description = "All in VPC"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project}-Elastic-File-System-SG-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}


locals {
  user_data_vars = {
    efs_mount_dns = aws_efs_mount_target.bupstart_mount.mount_target_dns_name
  }
}

resource "aws_efs_file_system" "bupstart_efs" {
  creation_token = "${var.project}-efs"

  tags = {
    Name        = "${var.project}-efs-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_efs_mount_target" "bupstart_mount" {
  file_system_id  = aws_efs_file_system.bupstart_efs.id
  subnet_id       = element(var.subnet_ids, 0)
  security_groups = [aws_security_group.efs_sg.id]
}

resource "aws_autoscaling_group" "autoscaling_group" {
  name                = "${var.project}-autoscaling-${var.environment}"
  max_size            = var.autoscaling_max_size
  min_size            = var.autoscaling_min_size
  desired_capacity    = var.autoscaling_desired_capacity
  vpc_zone_identifier = var.subnet_ids
  target_group_arns   = [var.lb_tg_arn]

  enabled_metrics = [ "GroupDesiredCapacity", "GroupInServiceCapacity", "GroupPendingCapacity", "GroupMinSize",
                      "GroupMaxSize", "GroupInServiceInstances", "GroupPendingInstances", "GroupStandbyInstances",
                      "GroupStandbyCapacity", "GroupTerminatingCapacity", "GroupTerminatingCapacity",
                      "GroupTotalCapacity", "GroupTotalInstances" ]

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
  }

  launch_template {
    id = aws_launch_template.autoscaling_template.id
  }
}

resource "aws_launch_template" "autoscaling_template" {
  name                   = "${var.project}-autoscaling-template-${var.environment}"
  image_id               = var.ami_id
  instance_type          = var.instance_type
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.instance_sg.id]
  user_data              = base64encode(templatefile("./user_data.sh", local.user_data_vars))
  update_default_version = true
  iam_instance_profile {
    name = "${var.project}-docker-logs-${var.environment}"
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name        = "${var.project}-autoscaling-instance-${var.environment}"
      Project     = var.project
      Environment = var.environment
    }
  }
}

# data "aws_launch_template" "autoscaling_template" {
#   name = "wordpress-template"
# }

resource "aws_autoscaling_policy" "autoscaling_policy_t1" {
  name                   = "${var.project}-autoscaling-policy-tier-1"
  autoscaling_group_name = aws_autoscaling_group.autoscaling_group.name
  policy_type            = "SimpleScaling"
  adjustment_type        = "ExactCapacity"
  scaling_adjustment     = 2
  cooldown               = 300
}

resource "aws_autoscaling_policy" "autoscaling_policy_t2" {
  name                   = "${var.project}-autoscaling-policy-tier-2"
  autoscaling_group_name = aws_autoscaling_group.autoscaling_group.name
  policy_type            = "SimpleScaling"
  adjustment_type        = "ExactCapacity"
  cooldown               = 300
  scaling_adjustment     = 3
}

resource "aws_autoscaling_policy" "autoscaling_policy_t3" {
  name                   = "${var.project}-autoscaling-policy-tier-3"
  autoscaling_group_name = aws_autoscaling_group.autoscaling_group.name
  policy_type            = "SimpleScaling"
  adjustment_type        = "ExactCapacity"
  cooldown               = 300
  scaling_adjustment     = 5
}

resource "aws_autoscaling_policy" "autoscaling_policy_t4" {
  name                   = "${var.project}-autoscaling-policy-tier-4"
  autoscaling_group_name = aws_autoscaling_group.autoscaling_group.name
  policy_type            = "SimpleScaling"
  adjustment_type        = "ExactCapacity"
  cooldown               = 300
  scaling_adjustment     = 8
}

resource "aws_cloudwatch_metric_alarm" "cloudwatch_alarm_up2" {
  alarm_actions       = [aws_autoscaling_policy.autoscaling_policy_t2.arn]
  alarm_name          = "${var.project}-cloudwatch-tier-2-up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "NetworkIn"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "10000000"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.autoscaling_group.name
  }

  tags = {
    Name        = "${var.project}-autoscaling-alarm-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_cloudwatch_metric_alarm" "cloudwatch_alarm_up3" {
  alarm_actions       = [aws_autoscaling_policy.autoscaling_policy_t3.arn]
  alarm_name          = "${var.project}-cloudwatch-tier-3-up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "NetworkIn"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "20000000"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.autoscaling_group.name
  }

  tags = {
    Name        = "${var.project}-autoscaling-alarm-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_cloudwatch_metric_alarm" "cloudwatch_alarm_up4" {
  alarm_actions       = [aws_autoscaling_policy.autoscaling_policy_t4.arn]
  alarm_name          = "${var.project}-cloudwatch-tier-4-up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "NetworkIn"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "30000000"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.autoscaling_group.name
  }

  tags = {
    Name        = "${var.project}-autoscaling-alarm-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_cloudwatch_metric_alarm" "cloudwatch_alarm_down3" {
  alarm_actions       = [aws_autoscaling_policy.autoscaling_policy_t3.arn]
  alarm_name          = "${var.project}-cloudwatch-tier-3-down"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "NetworkIn"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "30000000"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.autoscaling_group.name
  }

  tags = {
    Name        = "${var.project}-autoscaling-alarm-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_cloudwatch_metric_alarm" "cloudwatch_alarm_down2" {
  alarm_actions       = [aws_autoscaling_policy.autoscaling_policy_t2.arn]
  alarm_name          = "${var.project}-cloudwatch-tier-2-down"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "NetworkIn"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "20000000"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.autoscaling_group.name
  }

  tags = {
    Name        = "${var.project}-autoscaling-alarm-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_cloudwatch_metric_alarm" "cloudwatch_alarm_down1" {
  alarm_actions       = [aws_autoscaling_policy.autoscaling_policy_t1.arn]
  alarm_name          = "${var.project}-cloudwatch-tier-1-down"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "NetworkIn"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "10000000"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.autoscaling_group.name
  }

  tags = {
    Name        = "${var.project}-autoscaling-alarm-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_cloudwatch_metric_alarm" "cloudwatch_alarm_lb" {
  alarm_actions       = [aws_autoscaling_policy.autoscaling_policy_t2.arn]
  alarm_name          = "${var.project}-cloudwatchlb"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "RequestCount"
  namespace           = "AWS/LoadBalancer"
  period              = "60"
  statistic           = "Average"
  threshold           = "10000"

  dimensions = {
    LoadBalancer = var.load_balancer_arn
  }

  tags = {
    Name        = "${var.project}-autoscaling-instance-lb-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "${var.project}-docker-logs-${var.environment}"

  tags = {
    Name        = "${var.project}-log-group-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_iam_role" "logs-role" {
  name = "${var.project}-docker-logs-${var.environment}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name        = "${var.project}-logs-role-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

data "aws_iam_policy" "logs-policy" {
  arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
}

resource "aws_iam_role_policy_attachment" "logs-attach" {
  role       = aws_iam_role.logs-role.name
  policy_arn = data.aws_iam_policy.logs-policy.arn
}

resource "aws_iam_instance_profile" "logs_profile" {
  name = "${var.project}-docker-logs-${var.environment}"
  role = aws_iam_role.logs-role.name
}