resource "aws_security_group" "lb_sg" {
  name        = "${var.project}-LB-SG-${var.environment}"
  description = "Allow HTTPS from internet"
  vpc_id      = var.vpc_id

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project}-LoadBalancer-SG-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_lb_target_group" "bupstart-tg-http" {
  name     = "${var.project}-TG-HTTP-${var.environment}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  health_check {
    interval = 10
  }
}

resource "aws_lb" "bupstart-lb" {
  name               = "${var.project}-LoadBalancer-${var.environment}"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg.id]
  subnets            = var.subnets

  enable_deletion_protection = false

  tags = {
    Name        = "${var.project}-LoadBalancer-${var.environment}"
    Project     = var.project
    Environment = var.environment
  }
}

resource "aws_lb_listener" "bupstart_lb_listener" {
  load_balancer_arn = aws_lb.bupstart-lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.bupstart-tg-http.arn
  }
}