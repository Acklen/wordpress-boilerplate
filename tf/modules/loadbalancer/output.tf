output "lb_dns" {
  value = aws_lb.bupstart-lb.dns_name
}

output "lb_id" {
  value = aws_security_group.lb_sg.id
}

output "target_group_arn" {
  value = aws_lb_target_group.bupstart-tg-http.arn
}

output "load_balancer_arn" {
  value = aws_lb.bupstart-lb.arn
}