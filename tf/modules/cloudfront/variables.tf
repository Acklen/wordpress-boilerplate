variable "dns_name" {}
variable "environment" {}
variable "project" {}
variable "cert_arn" {}
variable "cloudfront_aliases" {}
variable "certificate_domain" {}